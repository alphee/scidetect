/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.Checker.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
/**
 * @author Nguyen Minh Tien minh-tien.nguyen@imag.fr
 */
public class ThresholdsSet extends HashMap<String, Double[]> {

    public ThresholdsSet() {
    		    super();
    }
    
    /**
     * Initialize the thresholds Set by reading the configuration file
     */
    public void Init() {
    	try{
    		this.readconfig();
    	}
    	catch (Exception E) { 
    		System.out.println("*****");
            System.out.println("***** Problem when reading Thresholds values");
            System.out.println("***** Please check the config.txt file ");
    		System.out.println("*****");
    	};
    }
    
    /**
     * Reads threshold in the configuration file (default config.txt).
     * @throws Exception 
     */
    private void readconfig() throws Exception {
        File conf = new File("config.txt");
        BufferedReader br = new BufferedReader(new FileReader(conf));
        String line;
        boolean foundClass= false;
        while ((line = br.readLine()) != null) {
            if (line.startsWith("Threshold_")) {
                // System.out.println(line);
            	foundClass=true;
                String[] b = line.split("\t");
                Double[] temp = new Double[2];
                temp[0] = Double.parseDouble(b[1]);
                temp[1] = Double.parseDouble(b[2]);
                this.put(b[0].substring(10, b[0].length()), temp);
                //10 because i want to cut Threshold_
            }
        }
        br.close();
        if (!foundClass){throw new Exception();};
    }


	
}
