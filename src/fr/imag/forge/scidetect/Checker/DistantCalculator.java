/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.Checker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import fr.imag.forge.scidetect.Checker.Utils.DistancesSet;
import fr.imag.forge.scidetect.Corpus.Corpus;

/**
 * Compute distances between two sets of texts
 *
 * @author Nguyen Minh Tien - minh-tien.nguyen@imag.fr
 */
public class DistantCalculator {

    //private HashMap<String, HashMap<String, Double>> distant = new HashMap<String, HashMap<String, Double>>();
    private DistancesSet distant = new DistancesSet();

    /**
     * Compute distances between each text of a corpus and the samples
     *
     * @param samples corpus
     * @param tests corpus
     * @return DistancesSet distances from text in test to text in sample
     */
    public DistancesSet caldistant(Corpus samples, Corpus tests) {
        for (String key : tests.keySet()) {
            //HashMap<String, Double> distantto = new HashMap<String, Double>();
            for (String key2 : samples.keySet()) {
                double distanttt = cal_textdistant(tests.get(key), samples.get(key2));
                // System.out.println("distant between " + key + " and " + key2
                // + ": " + distanttt);
                //distantto.put(key2, distanttt);
                distant.setDist(key, key2, distanttt);
            }
            //distant.put(key, distantto);
        }

        return distant;
    }

    /**
     * Compute the distance between 2 texts index
     *
     * @param text1
     * @param text2
     * @return the distance between text 1 and text 2.
     */
    private double cal_textdistant(HashMap<String, Integer> text1,
            HashMap<String, Integer> text2) {
        double nboftoken = 0.0;
        double sum = 0.0;

        Set<String> keys1 = text1.keySet();
        Set<String> keys2 = text2.keySet();
        Set<String> allkeys = new HashSet<String>();
        allkeys.addAll(keys1);
        allkeys.addAll(keys2);
        Integer Na = 0, Nb = 0;
        // get the nb of token in each text
        for (String key : allkeys) {
            Integer Fa = 0;
            Integer Fb = 0;
            if (text1.containsKey(key)) {
                Fa = text1.get(key);
            }
            if (text2.containsKey(key)) {
                Fb = text2.get(key);
            }
            Na += Fa;
            Nb += Fb;
        }
        // reduce propotion for text of different lenght
        if (Na <= Nb) {
            for (String key : allkeys) {
                Integer Fa = 0;
                Integer Fb = 0;
                if (text1.containsKey(key)) {
                    Fa = text1.get(key);
                }
                if (text2.containsKey(key)) {
                    Fb = text2.get(key);
                }
                sum += Math.abs(Fa - (double) Fb * (Na / (double) Nb));
            }
            return sum / (2 * Na);
        } else {
            for (String key : allkeys) {
                Integer Fa = 0;
                Integer Fb = 0;
                if (text1.containsKey(key)) {
                    Fa = text1.get(key);
                }
                if (text2.containsKey(key)) {
                    Fb = text2.get(key);
                }
                sum += Math.abs(Fa * (Nb / (double) Na) - (double) Fb);
            }
            return sum / (2 * Nb);
        }
    }
}
