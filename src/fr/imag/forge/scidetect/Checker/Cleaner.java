/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.Checker;

import fr.imag.forge.scidetect.Corpus.Text;
import fr.imag.forge.scidetect.Corpus.TextProcessor;
import java.io.File;
import java.util.ArrayList;

/**
 * Clean generated txt after processing
 *
 * @author Nguyen Minh Tien - minh-tien.nguyen@imag.fr
 */
public class Cleaner {

    public void clean(String foldername) {
        File folder = new File(foldername);

        if (folder.isDirectory()) {
            File[] listOfFile = folder.listFiles();
            for (int j = 0; j < listOfFile.length; j++) {
                if (listOfFile[j].isDirectory()) {
                    clean(listOfFile[j].getPath());
                } else if (listOfFile[j].getName().endsWith(".pdf") || listOfFile[j].getName().endsWith(".xml") || listOfFile[j].getName().endsWith(".xtx")) {
                    String originalname = listOfFile[j].getName();
                    String originalnamewoextension = originalname.substring(0, originalname.lastIndexOf("."));
                    for (int i = 0; i < listOfFile.length; i++) {
                        if (listOfFile[i].getName().endsWith(".txt")&&listOfFile[i].getName().contains(originalnamewoextension) && !listOfFile[i].getName().contains(originalname)&&!listOfFile[i].getName().contains(".pdf.xtx")) {
                            listOfFile[i].delete();
                        }
                    }
                }

            }
        } else if (folder.getName().endsWith(".pdf") || folder.getName().endsWith(".xml") || folder.getName().endsWith(".xtx")) {
            String originalname = folder.getName();
            File[] listOfFile = folder.getParentFile().listFiles();
            String originalnamewoextension = folder.getName().substring(0, folder.getName().lastIndexOf("."));
            for (int i = 0; i < listOfFile.length; i++) {
                if (listOfFile[i].getName().endsWith(".txt")&&listOfFile[i].getName().contains(originalnamewoextension) && !listOfFile[i].getName().contains(originalname)&&!listOfFile[i].getName().contains(".pdf.xtx")) {
                    listOfFile[i].delete();
                }
            }
        }

    }
}
