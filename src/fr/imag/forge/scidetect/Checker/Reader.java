/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.Checker;

//import com.sun.corba.se.spi.transport.CorbaAcceptor;
//import fr.imag.forge.scidetect.TextExtractor.Xmlextractor;
//import fr.imag.forge.scidetect.TextExtractor.pdfextractor;
import fr.imag.forge.scidetect.Checker.Utils.DistancesSet;
import fr.imag.forge.scidetect.Corpus.Corpus;
import fr.imag.forge.scidetect.Corpus.TextProcessor;
import fr.imag.forge.scidetect.Corpus.Text;
import fr.imag.forge.scidetect.Logger.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Nguyen Minh Tien - minh-tien.nguyen@imag.fr
 */
public class Reader {

    //private HashMap<String, HashMap<String, Integer>> samples = new HashMap<String, HashMap<String, Integer>>();
    // private HashMap<String, HashMap<String, Integer>> tests = new HashMap<String, HashMap<String, Integer>>();
    private Corpus samples = new Corpus();
    private Corpus test = new Corpus();
    private String SamplesFolder;

    /**
     * Read config file
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    public void readconfig() throws FileNotFoundException, IOException {
        File conf = new File("config.txt");
        BufferedReader br = new BufferedReader(new FileReader(conf));
        String line;

        while ((line = br.readLine()) != null) {
            if (!line.startsWith("#")) {
                // System.out.println(line);
                String[] b = line.split("\t");
                if (b[0].equals("samples")) {
                    SamplesFolder = b[1];
                }
                //other config should be read over here
                if (b[0].equals("Max_length")) {
                    //maxlength = Integer.parseInt(b[1]);
                    TextProcessor.maxlength = Integer.parseInt(b[1]);
                }
                if (b[0].equals("Min_length")) {
                    //maxlength = Integer.parseInt(b[1]);
                    TextProcessor.minlength = Integer.parseInt(b[1]);
                }
            }
        }
        br.close();

    }

    /**
     * Read the sample folder
     *
     * @param foldername
     * @return sample corpus
     * @throws IOException
     */
    public Corpus readsamples(String foldername) throws IOException {
        File folder = new File(foldername);
        File[] listOfFile = folder.listFiles();
        for (int j = 0; j < listOfFile.length; j++) {
            if (listOfFile[j].isDirectory()) {
                readsamples(listOfFile[j].getPath());
            } else if (listOfFile[j].getName().endsWith(".pdf") || listOfFile[j].getName().endsWith(".xml") || listOfFile[j].getName().endsWith(".xtx") || (listOfFile[j].getName().endsWith(".txt") && !listOfFile[j].getName().startsWith("INDEX-"))) {
                ArrayList<Text> text = new ArrayList<Text>();
                // System.out.println(listOfFile[j].getName());
                TextProcessor textprocessor = new TextProcessor();
                text = textprocessor.newtext(listOfFile[j], listOfFile);
                for (int i = 0; i < text.size(); i++) {
                    samples.put(text.get(i));
                }
            }

        }
        return samples;

    }

    /**
     * Read the test folder
     *
     * @param foldername
     * @return test corpus
     * @throws IOException
     */
    public String readtests(String foldername, Corpus Samplecorpus, Boolean savedetaillog) throws IOException {
        File folder = new File(foldername);
        String conclusion = new String();
        if (folder.isDirectory()) {
            File[] listOfFile = folder.listFiles();
            for (int j = 0; j < listOfFile.length; j++) {
                if (listOfFile[j].isDirectory()) {
                    readtests(listOfFile[j].getPath(), Samplecorpus, savedetaillog);
                } else if (listOfFile[j].getName().endsWith(".pdf") || listOfFile[j].getName().endsWith(".xml") || listOfFile[j].getName().endsWith(".xtx")) {
                    ArrayList<Text> text = new ArrayList<Text>();
                    //System.out.println(listOfFile[j].getName());
                    TextProcessor textprocessor = new TextProcessor();
                    text = textprocessor.newtext(listOfFile[j], listOfFile);
                    for (int i = 0; i < text.size(); i++) {
                        test.put(text.get(i));
                    }

                }

            }
        } else if (folder.getName().endsWith(".pdf") || folder.getName().endsWith(".xml") || folder.getName().endsWith(".xtx")) {
            ArrayList<Text> text = new ArrayList<Text>();
            TextProcessor textprocessor = new TextProcessor();
            File[] listOfFile = folder.getParentFile().listFiles();
            //listOfFile[0] = folder;

            text = textprocessor.newtext(folder, listOfFile);
            for (int i = 0; i < text.size(); i++) {
                test.put(text.get(i));
            }

        }
        DistancesSet distant = new DistancesSet();
        DistantCalculator dc = new DistantCalculator();
        distant = dc.caldistant(Samplecorpus, test);
        Classifier cl = new Classifier();
        conclusion = cl.classify(distant);
        System.out.println(conclusion);
        Log log = new Log();
        log.savelog(conclusion);

        if (savedetaillog) {
            log.savedetaillog(distant);
        }
        return conclusion;

    }

}
