/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.Checker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 *Index texts (i.e. for each word computes its occurrence number)
 * @author Nguyen Minh Tien - minh-tien.nguyen@imag.fr
 */
public class Indexer {

    private Object content;

    /**
     * Index a text (count how many time each word is appearing)
     * and writes results in a file INDEX-filename.txt
     * @param content
     * @param textfile
     * @throws FileNotFoundException
     */
    public void index(String content, File textfile) throws FileNotFoundException {
       String filename = textfile.getName();
       //String filename = textfile.getName().substring(0,textfile.getName().lastIndexOf("."));
        filename+=".txt";
        String path = textfile.getParent();

        String[] words = content.split(" ");
        //System.out.println(words.length);
        HashMap<String, Integer> counter = new HashMap<String, Integer>();
        for (int i = 0; i < words.length; i++) {
            if (!counter.containsKey(words[i])) {
                counter.put(words[i], 1);
            } else {
                counter.put(words[i], counter.get(words[i]) + 1);
            }
        }

        File indexout = new File(path + "/INDEX-" + filename);
       // String filepath = (indexout.getPath());
        PrintWriter out = new PrintWriter(indexout);

        for (String key : counter.keySet()) {
            out.println(key + "\t" + counter.get(key));
        }
        out.close();
    }
}
