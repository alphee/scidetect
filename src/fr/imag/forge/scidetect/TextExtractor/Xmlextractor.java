/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.TextExtractor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Extract txt from a XML file
 * @author Nguyen Minh Tien - minh-tien.nguyen@imag.fr
 */
public class Xmlextractor {

    private String text = "";
    /**
     *  <#Description#>
     *
     *  @param nodeList <#nodeList description#>
     */
    private void printNote(NodeList nodeList) {

        for (int count = 0; count < nodeList.getLength(); count++) {

            Node tempNode = nodeList.item(count);

            // make sure it's element node.
            if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                if (tempNode.getNodeName() == "Para" || tempNode.getNodeName() == "PDFTextExtract") {
                    text += tempNode.getTextContent();
                }
                if (tempNode.hasChildNodes()) {
                    // loop again if has child nodes
                    printNote(tempNode.getChildNodes());
                }
            }
        }
        //return text;
    }
    /**
     *  Extract txt from xml
     *
     *  @param xml xml File
     *
     *  @return extracted content
     * @throws java.io.IOException
     */
    public String xmlextract(File xml) throws IOException {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            org.w3c.dom.Document doc = dBuilder.parse(xml);

            //System.out.println(doc.getDocumentElement().getNodeName());
            if (doc.hasChildNodes()) {

                printNote(doc.getChildNodes());

            }

        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Xmlextractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(Xmlextractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Xmlextractor.class.getName()).log(Level.SEVERE, null, ex);
        }
        File totxt = new File(xml.getPath()
                .substring(0, xml.getPath().lastIndexOf('.')) + ".txt");
        // File txt = new File(xml.getParent()+xml.getName().substring(0,xml.getName().lastIndexOf("."))+".txt");
            PrintWriter out = new PrintWriter(new FileWriter(totxt));
            out.println(text);
            out.close();
             normalizer a = new normalizer();
        String content = a.normalize(totxt);
         
        return content;
    }
}
