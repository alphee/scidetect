/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.TextExtractor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;

/**
 * Extract raw txt from a pdf File
 * @author Nguyen Minh Tien - minh-tien.nguyen@imag.fr
 */
public class pdfextractor {
    /**
     *  Extracts raw txt from a pdf file. The extracted txt is written in a File having the same name but with a .txt extension
     *  The file StdErrPDFExtractor.txt contains sdterr messages from org.apache.pdfbox.util.PDFTextStripper
     *  @param pdf a pdf File
     *
     *  @return a string containing the extracted text.
     * @throws java.io.IOException
     */
    public String pdfextract(File pdf) throws IOException {

        PDFTextStripper stripper = new PDFTextStripper("UTF-8");
        PDDocument pd;
        BufferedWriter wr;
        System.out
                .println("Converting: " + pdf.getPath());
        File totxt = new File(pdf.getPath()
                .substring(0, pdf.getPath().lastIndexOf('.')) + ".txt");

        try {
        	
            pd = PDDocument.load(pdf.getPath());
            wr = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(totxt)));
            try {
            	// dirty redirection of stderr because stripper.writeText is complaining
            	PrintStream orgStream   =  System.err;
            	PrintStream fileStream = new PrintStream(new FileOutputStream("StdErrPDFExtractor.txt",true));
            	System.setErr(fileStream);
            	System.err.println("* Sterr txt extraction from file:"+pdf.getPath());
            	stripper.writeText(pd, wr);
            	System.setErr(orgStream);
            	}
            catch (Exception e) {
                System.out.println("* Something went wrong during:");
                System.out.println(" - txt extraction from pdf:"+pdf.getPath());
                System.out.println("* Continuing anyway...");
            }
            if (pd != null) {
                pd.close();
            }
            // I use close() to flush the stream.
            wr.close();
        } catch (Exception e) {
            System.out.println("* Something went wrong during:");
            System.out.println(" - txt extraction from pdf:"+pdf);
            System.out.println("* Continuing anyway...");
        }
	//this seems to be faster but it seems like the app server does not support pdftotext
        //commandexecutor cm = new commandexecutor();
        //cm.execute("pdftotext "+ listOfFile[j].getPath());

        // ok now I have the txt file; lets normalize it
        normalizer a = new normalizer();
        String content = a.normalize(totxt);
        return content;
    }

}
