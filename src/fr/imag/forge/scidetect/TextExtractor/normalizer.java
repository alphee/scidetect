/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.TextExtractor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Performs normalization and cleaning of a dirty text. For example to clean a text extracted from a pdf file. 
 *
 * @author Nguyen Minh Tien - minh-tien.nguyen@imag.fr
 */

public class normalizer {
    /**
     *  Perform a text cleaning and normalization: all the text is transform in upper case. 
     *  non A to Z character are removed.
     *
     *  @param txt the File to be cleaned and normalized the resulting text is overwrites the original text.
     *
     *  @return contains all the normalized text.
     * @throws java.io.IOException
     */
    
    public String normalize(File txt) throws IOException {
        BufferedReader br;
        br = new BufferedReader(new FileReader(txt));
        String line;
        String content = "";
        while ((line = br.readLine()) != null) {
            content += " ";
            content += line;

        }
        br.close();
        content = content.toUpperCase();
        content = content.replaceAll("-", " ");// parenthesis
        content = content.replaceAll("[^A-Z ]", "");// non A to Z
        content = content.replaceAll("\r", " "); // make a new line
        content = content.replaceAll("\n", " ");//prob not nessesary :D
        content = content.replaceAll("\\s+", " ");// remove extra spaces\
       //content = content.replaceAll("[-\r\n\\s+]", " ");// parenthesis
        
        //content = content.replaceAll("[^A-Z ]", "");// remove non A to Z
        PrintWriter out = new PrintWriter(txt);
        out.println(content);
        out.close();
        return content;
    }

}
