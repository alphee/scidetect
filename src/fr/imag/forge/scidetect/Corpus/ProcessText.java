/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.Corpus;

import fr.imag.forge.scidetect.Checker.Indexer;
import fr.imag.forge.scidetect.TextExtractor.Xmlextractor;
import fr.imag.forge.scidetect.TextExtractor.pdfextractor;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Manage texts file in the corpus
 *
 * @author Nguyen Minh Tien - minh-tien.nguyen@imag.fr
 */
public class ProcessText {

    /**
     *
     */
    public static int maxlength;
    ArrayList<Text> text = new ArrayList<Text>();

    /**
     * Process a File (pdf,xml) to create clean set of text (incase of need to split)
     *
     * @param original file
     * @param listOfFile
     * @return list of text[]
     * @throws java.io.IOException
     *
     */
    public ArrayList<Text> newtext(File original, File[] listOfFile) throws IOException {
        // find if there is already index for it
        String indexname = "INDEX-"
                + original.getName().substring(0,
                        original.getName().lastIndexOf("."))
                + ".txt";
        String content = "";
        if (Arrays.asList(listOfFile).toString().contains(indexname)) {
            // System.out.println("lets read from index file");
            readindexfile(original.getParent() + "/" + indexname);
        } else {
            if (original.getName().endsWith(".pdf")) {
                try {
                    pdfextractor a = new pdfextractor();
                    content = a.pdfextract(original);

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ProcessText.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (original.getName().endsWith(".xml") || original.getName().endsWith(".xtx")) {
                Xmlextractor a = new Xmlextractor();
                content = a.xmlextract(original);

            }
        //lets deal with long file over here
            //split content and the index part by part
            if (content.length() < maxlength) {

                Indexer b = new Indexer();
                b.index(content, original);

                readindexfile(original.getParent() + "/" + indexname);
            } else {
                String[] part = splitcontent(content);
                for (int i = 0; i < part.length; i++) {
                    String indexnameparti = "INDEX-"
                            + original.getName().substring(0,
                                    original.getName().lastIndexOf("."))
                            + "_part" + i + ".txt";
                    String filename = original.getName().substring(0,
                            original.getName().lastIndexOf("."))
                            + "_part" + i + ".txt";
                    Indexer b = new Indexer();
                    File a = new File(original.getParent() + "/" + filename);
                    PrintWriter out = new PrintWriter(new FileWriter(a));
                    out.println(part[i]);
                    //System.out.println(text);
                    out.close();
                    b.index(part[i], a);

                    readindexfile(a.getParent() + "/" + indexnameparti);

                }
            }
        }
        return text;

    }
/**
 * Read the index file if it is avaiable
 * @param path to the index file
 * @return hashmap of indexs
*/
    private HashMap<String, Integer> readindexfile(String path) throws IOException {
        File index = new File(path);
        BufferedReader br;
        br = new BufferedReader(new FileReader(index));
        String line;
        HashMap<String, Integer> a = new HashMap<String, Integer>();
        while ((line = br.readLine()) != null) {
            String[] b = line.split("\t");
            a.put(b[0], Integer.parseInt(b[1]));
        }
        br.close();
        Text c = new Text();
        c.setindex(a);
        c.setname(path);
        text.add(c);
        return a;
    }

    private String[] splitcontent(String content) {

        int nbofpart = content.length() / maxlength;
        String[] part = new String[nbofpart + 1];
        int lower = 0;
        int upper = 0;
        int i;
        for (i = 0; i < nbofpart; i++) {
            upper += maxlength;
            part[i] = content.substring(lower, upper);
            lower = upper;
        }

        if (upper <= content.length() - 1) {

            lower = upper;

            upper = content.length();

            part[i] = (content.substring(lower, upper));
        }
        return part;
    }

}
